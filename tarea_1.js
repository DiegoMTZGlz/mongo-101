/*  2) El conjunto de datos contiene 4 calificaciones de n estudiantes. 
Confirma que se importo correctamente la colección con los siguientes 
comandos en la terminal de mongo: ¿Cuántos registros arrojo el comando count?

    -Comando:
        $ db.grades.count();

    -Respuesta:
        800

    3) Encuentra todas las calificaciones del estudiante con el id numero 4.

    -Comando:
        $ db.grades.find({student_id:4});

    -Respuesta:
        [
            {
            _id: ObjectId("50906d7fa3c412bb040eb587"),
            student_id: 4,
            type: 'exam',
            score: 87.89071881934647
            },
            {
            _id: ObjectId("50906d7fa3c412bb040eb589"),
            student_id: 4,
            type: 'homework',
            score: 5.244452510818443
            },
            {
            _id: ObjectId("50906d7fa3c412bb040eb58a"),
            student_id: 4,
            type: 'homework',
            score: 28.656451042441
            },
            {
            _id: ObjectId("50906d7fa3c412bb040eb588"),
            student_id: 4,
            type: 'quiz',
            score: 27.29006335059361
            }
        ]

    4) ¿Cuántos registros hay de tipo exam?

    -Comando:
        $ db.grades.find({type:'exam'}).count();

    -Respuesta:
        200
                
    5) ¿Cuántos registros hay de tipo homework?

    - Comando: 
        $ db.grades.find({type:'homework'}).count();

    - Respuesta:
        400

    6) ¿Cuántos registros hay de tipo quiz?

    -Comando:
        db.grades.find({type:'quiz'}).count();

    -Respuesta:
        200

    7) Elimina todas las calificaciones del estudiante con el id numero 3

    -Comando:
        $ db.grades.deleteMany({student_id: 3});

    -Respuesta:
        { acknowledged: true, deletedCount: 4 }

    8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?

    -Comando:
        $ db.grades.find({score: 75.29561445722392});

    -Respuesta:
        [
            {
            _id: ObjectId("50906d7fa3c412bb040eb59e"),
            student_id: 9,
            type: 'homework',
            score: 75.29561445722392
            }
        ]  

    9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    
    -Comando:
        $ db.grades.update({_id:ObjectId("50906d7fa3c412bb040eb591")},{$set:{"score":100}});
    
    -Respuesta:
        {
            acknowledged: true,
            insertedId: null,
            matchedCount: 1,
            modifiedCount: 1,
            upsertedCount: 0
        }

    10) A qué estudiante pertenece esta calificación.

    -Comando:
        $ db.grades.find({_id:ObjectId("50906d7fa3c412bb040eb591")});
    
    -Respuesta:
        [
            {
            _id: ObjectId("50906d7fa3c412bb040eb591"),
            student_id: 6,
            type: 'homework',
            score: 100
            }
        ] 

*/