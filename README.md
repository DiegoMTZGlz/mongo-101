# Tarea | Mongo 101

En esta tarea vamos a utilizar una colección de calificaciones de estudiantes, todas las respuestas se colocaran en un archivo llamado tarea_1.js, utilizando comentarios, se colocaran las preguntas, luego los comandos utilizados y de nuevo en comentarios las respuestas.

### Requisitos previos

- [mongodb](https://www.mongodb.com/try/download/community-edition)

### Instalación

Instalaremos gnupg y curl usando:

```
sudo apt-get install gnupg curl
```

Importaremos la llave pública GPG de MongoDB usando:

```
curl -fsSL https://pgp.mongodb.com/server-7.0.asc | \
   sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
   --dearmor
```

Recargaremos los paquetes locales con:

```
sudo apt-get update

```

Instalaremos MongoDB con:

```
sudo apt-get install -y mongodb-org
```

Iniciaremos el proceso de MonboDB para que pueda ser utilizado con:

```
sudo systemctl start mongod
```

Ahora podemos importar la BD para el ejercicio con:

```
mongoimport -d students -c grades < grades.json
```

Ahora ejecutaremos el shell de mongo con:

```
mongosh
```

Finalmente accederemos a nuestra base de datos con:

```
use students
```

## Pruebas

Ejecutamos un count para ver el total de registros:

```
db.grades.count();
```

La terminal debería retornarnos la siguiente respuesta:

```
> 800
```

## Hecho con

  - [MongoDB](https://www.mongodb.com/es) - Usado para almacenar los datos

## Authors

  - **Diego Martínez** -
    [DiegoMTZ](https://gitlab.com/DiegoMTZGlz)